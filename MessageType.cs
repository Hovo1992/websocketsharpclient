﻿namespace SocketClient
{
    public enum MessageType
    {
        None,
        Message,
        NewConnectedName,
        NameNotification
    }
}
