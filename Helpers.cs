﻿using Newtonsoft.Json;

namespace SocketClient
{
    public static class Helpers
    {
        public static string SerialiseObject(object data) => JsonConvert.SerializeObject(data);
    }
}
