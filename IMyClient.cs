﻿using System;

namespace SocketClient
{
    interface IClient
    {
        void Connect(string name);
        void SendMessage(string message);

        event Action<string> OnRecive;
        event Action<MessageModel> OnNewClientConneted;
    }
}
