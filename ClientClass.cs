﻿using System;
using WebSocketSharp;
using Newtonsoft.Json;
using WebSocketSharp.Net;
using System.Collections.Generic;

namespace SocketClient
{
    public class ClientClass : IClient
    {
        #region Private Fields
                
        private readonly string _port;
        private readonly string _domain;
        private readonly string _service;

        private readonly WebSocket _ws;
        private readonly Dictionary<MessageType, Action<MessageModel>> _receivers;

        private Cookie _userInfo;

        #endregion

        #region Events

        public event Action<string> OnRecive;
        public event Action<MessageModel> OnNewClientConneted;

        #endregion

        #region Constructor

        public ClientClass(string domain, string port, string service)
        {
            _port = port;
            _domain = domain;
            _service = service;

            _receivers = new Dictionary<MessageType, Action<MessageModel>>
            {
                { MessageType.Message, ForMessage },
                { MessageType.NewConnectedName, ForNewConnectedName },
            };

            _ws = new WebSocket($"ws://{_domain}:{_port}/{_service}");
            _ws.OnMessage += MessageRecieve;            
        }

        #endregion

        #region Interface Methods
        
        public void Connect(string name)
        {
            _userInfo = new Cookie
            {
                Name = "UserInfo",
                Value = name,
                Domain = _domain,
                Comment = "commentmessage",
                Expires = DateTime.UtcNow.AddDays(1)
            };

            _ws.SetCookie(_userInfo);
            _ws.Connect();
        }

        public void SendMessage(string message)
        {
            var data = new MessageModel
            {
                MessageType = (int)MessageType.Message,
                Message = message,
                SenderName = _userInfo.Value
            };

            var jsonMessage = Helpers.SerialiseObject(data);
            _ws.Send(jsonMessage);
        }

        #endregion

        #region Websocket Client Event Handlers

        public void MessageRecieve(object sender, MessageEventArgs e)
        {
            var messageModel = JsonConvert.DeserializeObject<MessageModel>(e.Data);

            _receivers[(MessageType)messageModel.MessageType].Invoke(messageModel);
        }

        #endregion

        #region Receiver Actions

        private void ForMessage(MessageModel messageModel) =>
            OnRecive?.Invoke($"{DateTime.Now.ToShortTimeString()} - {messageModel.SenderName}:\n" +                             
                             $"{messageModel.Message}\n");

        private void ForNewConnectedName(MessageModel messageModel) => OnNewClientConneted?.Invoke(messageModel);

        #endregion
    }
}