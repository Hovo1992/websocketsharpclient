﻿using System;
using System.Windows;

namespace SocketClient
{
    public partial class MainWindow : Window
    {
        static IClient _clientClass;

        public MainWindow()
        {
            InitializeComponent();

            Dispatcher.Invoke(() => _clientClass = new ClientClass(DomainName.Text, Port.Text, "ServerChat"));

            _clientClass.OnRecive += PrintMessage;
            _clientClass.OnNewClientConneted += UpdateClientsList;          
        }

        public void PrintMessage(string message) => Dispatcher.Invoke(() => MessageArea.Text += message + Environment.NewLine);

        public void UpdateClientsList(MessageModel data) => Dispatcher.Invoke(() => UsersList.Items.Add(data.SenderName));

        private void ConnectButton_Click(object sender, RoutedEventArgs e) => _clientClass.Connect(ClientName.Text);

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(() => _clientClass.SendMessage(InputMessage.Text));
            InputMessage.Text = string.Empty;
        }
    }
}